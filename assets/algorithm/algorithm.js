export const algo = `

const RenderRow =({rowItem, rowKey}) => {
    return (
      <div key={rowKey} style={styles.row}>
        <div style={styles.itemTitle}>{rowItem.name}</div>
        <div style={styles.itemQty}>{rowItem.qty}</div>
      </div>
    )
  }

const RenderCategory = ({category, categoryKey}) => {
return (
<div key={categoryKey}>
  <p style={styles.categoryHeader}>{category.title}</p>
  {category.data.map((rowItem, rowKey) => RenderRow({rowItem, rowKey}) )}
</div>
)}

const prepareData = () => {
    /*return Object.entries(this.props.store.default.list).map(el=>({
      title: el[0],
      data: el[1].map(i => {
        if(i['packed'] === undefined) i['packed'] = false;
        return i;
      })
    }));*/
    return [{title: 'test',data: [{name: 'testname', qty: 1, packed: false}]}];
  }

const RenderList = () => {
  let data = [{title: 'tests',data: [{name: 'testname', qty: 1, packed: false}]}];
    return (
      data.map((category, categoryKey) => RenderCategory({category, categoryKey: 'categoryKey'})
    ))
}

const styles = {
  heading: {
    fontSize: "2.25rem",
    fontWeight: "bold"
  },
  copy: {
    fontSize: "1.5rem"
  },
  container: {
    flex: 1,
    backgroundColor: '#F5FCFF',
  },
  categoryHeader: {
    textAlign: 'left',
    fontSize: 20,
    paddingLeft: 10,
    fontWeight: 'bold',
    backgroundColor: 'rgba(240,240,240,1.0)',
    elevation: 2
  },
  itemTitle: {
    flex: 1,
    padding: 10,
    fontSize: 18,
    height: 44,
  },
  itemQty: {
    padding: 10,
    fontSize: 18,
    height: 44,
    alignSelf: 'flex-end'
  },
  row: {
    display:'flex',
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignContent: 'center',
    alignItems: 'center'
  }
}

const RrenderThings = () => (
  <div style={{display: 'flex', flexDirection: 'column'}}>
    aaaaaaaa
  </div>
)

render(<RenderList />)
`