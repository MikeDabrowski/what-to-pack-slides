// Import React
import React from "react";
// Import Spectacle Core tags
import {
  Appear,
  BlockQuote,
  Cite,
  CodePane,
  ComponentPlayground,
  Deck,
  Fill,
  GoToAction,
  Heading,
  Image,
  Layout,
  Link,
  List,
  ListItem,
  Markdown,
  MarkdownSlides,
  Quote,
  Slide,
  SlideSet,
  Table,
  TableBody,
  TableHeader,
  TableHeaderItem,
  TableItem,
  TableRow,
  Text
} from "spectacle";
import createTheme from "spectacle/lib/themes/default";
import preloader from "spectacle/lib/utils/preloader";
import { algo } from '../assets/algorithm/algorithm';
import '../assets/custom-styles.css';
import Interactive from '../assets/interactive';

const questions = `
Pytania na koniec czy mogę pozwolic zadawac je w trakcie? Jak to zwykle bywa? Mi specjalnie nie przeszkadza w trakcie, jednak to na pewno wydłuża prezentację.
Czy mam czas na sprawdzenie sprzętu? Jaka sala? 
`

// Require CSS
require("normalize.css");
const images = {
  tools: {
    gitLab: require('../assets/tools/GitLab_Logo.svg.png'),
    idea: require('../assets/tools/IDEA.png'),
    vbox: require('../assets/tools/vbox.png'),
    vs: require('../assets/tools/vs2.jpg'),
    xcode: require('../assets/tools/xcode.png')
  },
  techs: {
    android: require('../assets/techs/android.png'),
    cordova: require('../assets/techs/cordova_256.png'),
    ios: require('../assets/techs/IOS_11_logo.png'),
    nativeScript: require('../assets/techs/NativeScript_logo.png'),
    sencha: require('../assets/techs/sencha.png'),
    titanium: require('../assets/techs/titanium_logo.png'),
    weex: require('../assets/techs/weex.png'),
    windows: require('../assets/techs/windows.jpg'),
    xamarin: require('../assets/techs/xamarin.svg'),
  },
  rn: {
    welcome: {
      windows: require('../assets/RN/01-react-native-welcome-ios-opt.png'),
      ios: require('../assets/RN/01-react-native-welcome-ios-opt.png'),
      android: require('../assets/RN/02-react-native-welcome-android-opt.png')
    },
    wtp1: require('../assets/RN/wtp-1.png'),
    wtp2: require('../assets/RN/wtp-2.png'),
    wtp3: require('../assets/RN/wtp-3.png'),
    wtp4: require('../assets/RN/wtp-4.png'),
    wtp5: require('../assets/RN/wtp-5.png'),
    wtp6: require('../assets/RN/wtp-6.png')
  },
  frameworks: {
    react: require('../assets/frameworks/react.png'),
    redux: require('../assets/frameworks/redux.png')
  },
  backgrounds: {
    airport1: require('../assets/backgrounds/airport1.jpg'),
    airport2: require('../assets/backgrounds/airport2.png'),
    airport3: require('../assets/backgrounds/airport3.jpg'),
    lockers: require('../assets/backgrounds/lockers.jpg'),
    travels: require('../assets/backgrounds/travels.jpg'),
    metro: require('../assets/backgrounds/metro.png'),
    fb: require('../assets/backgrounds/fb.jpg'),
    reactWall: require('../assets/backgrounds/react-wall.jpg'),
    reduxWall: require('../assets/backgrounds/redux-wall.png'),
    androbot: require('../assets/backgrounds/android-svgrepo-com.svg'),
    w10: require('../assets/backgrounds/w10.jpg'),
    sierra: require('../assets/backgrounds/sierra.jpg'),
  },
  thesis: {
    algoOverview: require('../assets/thesisImgs/algorithm-overview-horizontal.svg'),
    newTripScreen: require('../assets/results/newTrip.png'),
    packingListScreen: require('../assets/results/packingList.png'),
    windows1: require('../assets/results/win-1.jpg'),
    windows2: require('../assets/results/win2.jpg'),
    ios1: require('../assets/results/ios-real-1.jpg'),
    ios2: require('../assets/results/ios-real-2.jpg'),
    vboxios1: require('../assets/results/vbox-ios-1.png'),
    vboxios2: require('../assets/results/vbox-ios-2.png'),
    android1: require('../assets/results/xiaomi1.png'),
    android2: require('../assets/results/xiaomi2.png')
  }
};
preloader(images);
const theme = createTheme({
  primary: "white",
  secondary: "#03A9FC",
  tertiary: "#1F2022",
  quarternary: "#CECECE",
  orange: '#FFA500',
  lightOrange: '#ffdb71',
  bgReact: '#01191f',
  bgRedux: '#13011f',
  bgReduxAlt: '#9900ff'
}, {
  primary: "Montserrat",
  secondary: "Helvetica"
});

const notes = {
  intro: 'Przedstaw się, pytania na koniec czy w trakcie?',
  goals: `
    <ul>
    <li>firma bedzie miec projekt w RN</li>
    <li>Zawsze mam problem z pakowaniem się</li>
    <li>interesuję się tym (i FE ogólnie)</li>
    <li>chce się douczyć</li>
    <li>aplikacja w js na kompa</li>
    </ul>
     Jak ma działać taka aplikacja? - potrzebne jest stworzenie algorytmu
    `,
  algo: `
    <ul>
        <li>Alogrytm stworzony przeze mnie</li>
        <li>Tworzony z myslą o wszechstronności</li>
    </ul>
    `,
  algoAssumptions: `Projektując algorytm, chiałem by poza swoją użytecznością, był też łatwy w obsłudze i rozbudowie.
   Chciałem by dodawanie kolejnych przedmiotów nie pociągało za sobą zmian w algorytmie - to było łatwe. 
   Trudniej było z kategoriami, które są mocniej związane z algorytmem`,
  algoDifficulties: `Już na etapie wybierania technologii wiedziałem, że mogą pojawić się pewne trudności w działąniu lub implementacji algorytmu.
  Wyróżniłem tu najważniejsze z nich. Z braku dostępnych rozwiązań na których mógłbym się wzorować musiałem opierać sięna własnych doswiadczeniach.
  Dlatego też części algorytm są w niewielkim stopniu spersonalizowane pode mnie (obliczenia). `,
  algoOverview: `Opowiedzieć o tym że ma mieszane podejście - czasem bierze wszystko i odrzuca niepasujące czasem dobiera pasujące. 
    Widać to na wieku (odrzut) i kategoriach (dodawanie)`,
  algoSteps: `Opowiedziec o modularności`,
  algoUniversal: ``,
  data: ``,
  reactNative: ``,
  redux: ``,
  screens: ``,
  platforms: ``,
  improvements: ``,
};

export default class Presentation extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      steps: 0
    }
  }

  updateSteps = steps => {
    if (this.state.steps !== steps) { // eslint-disable-line no-invalid-this
      this.setState({steps}); // eslint-disable-line no-invalid-this
    }
  };

  render() {
    return (
      <Deck transition={["slide"]} transitionDuration={500} theme={theme} contentWidth={1000}>

        {/* Intro slide */}
        <Slide transition={['slide']}
               bgImage={images.backgrounds.airport2.replace('/', '')} bgDarken={0.75}
               notes={notes.intro}>
          <Heading size={1} fit caps lineHeight={1} textColor="white">
            What To Pack
          </Heading>
          {/*<Heading size={1} fit caps>Luggage packing made easy</Heading>*/}
          <Heading size={2} fit caps>Prosty sposób na pakowanie</Heading>
          <Link href="http://dabrowski.tk">
            <Text textSize=".85em" textColor="white" margin="20px 0px 0px" bold>Student: Michał Dąbrowski</Text>
          </Link>
          <Text textSize=".85em" textColor="white" margin="20px 0px 0px" bold>Promotor: dr inż. Dominik Olszewski</Text>
        </Slide>

        {/* About thesis */}
        <Slide
          notes={notes.goals}>
          <Heading caps>Cele pracy</Heading>
          <div style={{display: 'flex', justifyContent: 'center', textAlign: 'left'}}>
            <List>
                <Appear><ListItem>Zdobycie doświadczenia w React-Native</ListItem></Appear>
              <Appear><ListItem>Ułatwienie pakowania się</ListItem></Appear>
              <Appear><ListItem>Stworzenie aplikacji mobilnej</ListItem></Appear>
              <Appear><ListItem>Jeden kod - wszystkie platformy</ListItem></Appear>
            </List>
          </div>
        </Slide>

        {/* Algorytm example */}
        {/*<Slide transition={['slide']} bgColor="primary" notes={notes.algo}>
          <Heading caps textColor="black" fit size={6} style={{flexDirection: 'row', justifyContent: 'center'}}>
            Alogrytm
          </Heading>
          <Heading size={1} fit caps>
            A ReactJS Presentation Library
          </Heading>
          <Heading size={1} fit caps>
            Where You Can Write Your Decks In JSX
          </Heading>
          <Link href="https://github.com/FormidableLabs/spectacle">
            <Text bold caps textColor="secondary">View on Github</Text>
          </Link>
          <Text textSize="1.5em" margin="20px 0px 0px" bold>Hit Your Right Arrow To Begin!</Text>
        </Slide>*/}

        {/* Algorytm - założenia */}
        <Slide>
          <Heading size={1} fit notes={notes.algoAssumptions} caps>Algorytm - założenia</Heading>
          <div style={{display: 'flex', justifyContent: 'center', textAlign: 'left'}}>
            <List>
              <Appear><ListItem>Uniwersalność</ListItem></Appear>
              <Appear><ListItem>Łatwość dodawania nowych przedmiotów</ListItem></Appear>
              <Appear><ListItem>Wykorzystanie kategorii przedmiotów</ListItem></Appear>
            </List>
          </div>
        </Slide>

        {/* Algorytm - trudności */}
        <Slide>
          <Heading size={1} fit notes={notes.algoDifficulties} caps>Algorytm - trudności</Heading>
          <div style={{display: 'flex', justifyContent: 'center', textAlign: 'left'}}>
            <List>
              <Appear><ListItem>Mało lub brak gotowych rozwiązań</ListItem></Appear>
              <Appear><ListItem>Potencjalnie duża ilość kombinacji parametrów</ListItem></Appear>
              <Appear><ListItem>Opieranie się na własnych doświadczeniach</ListItem></Appear>
              <Appear><ListItem>Trudności obliczenia ilości przedmiotów</ListItem></Appear>
              <Appear><ListItem>Potencjalne problemy wydajnościowe</ListItem></Appear>
              <Appear><ListItem>Brak bazy danych</ListItem></Appear>
            </List>
          </div>
        </Slide>

        {/* Przechowywanie danych */}
        <Slide notes={notes.data}
               bgImage={images.backgrounds.lockers.replace('/', '')} bgDarken={0.75}>
          <Heading size={1} fit textColor="white" caps>Przechowywanie danych</Heading>
          <Heading size={4} textColor="secondary">Kategorie</Heading>
          <CodePane
            textSize={16}
            lang="jsx"
            source={require('raw-loader!../assets/snippets/category.snip')}
            margin="20px auto"
            overflow="overflow"
          />
        </Slide>

        {/* Przechowywanie danych */}
        <Slide notes={notes.data}
               bgImage={images.backgrounds.lockers.replace('/', '')} bgDarken={0.75}>
          <Heading size={1} fit textColor="white" caps>Przechowywanie danych</Heading>
          <Heading size={4} textColor="secondary">Przedmioty</Heading>
          <CodePane
            textSize={20}
            lang="jsx"
            source={require('raw-loader!../assets/snippets/item.snip')}
            margin="20px auto"
            overflow="overflow"
          />
        </Slide>

        {/* Algorytm - ogólnie */}
        <Slide notes={notes.algoSchematic}
               bgImage={images.backgrounds.metro.replace('/','')} bgDarken={0.75}>
          <Heading size={1} fit  textColor="white" caps>Schemat algorytmu</Heading>
          <Image style={{marginTop: 100, width: '100%', backgroundColor: 'white', padding: '40px'}} src={images.thesis.algoOverview}/>
        </Slide>

        {/* Algorytm - kroki */}
        <Slide notes={notes.algoSteps}>
          <Heading size={1} fit caps>Kroki algorytmu</Heading>
          <CodePane
            textSize={20}
            lang="jsx"
            source={require('raw-loader!../assets/snippets/mainAlgo.snip')}
            margin="20px auto"
            overflow="overflow"
          />
        </Slide>

        {/* Algorytm - modularność
        <Slide notes={notes.algoModularity}>
          <Heading size={1} fit>Modularność algorytmu</Heading>
          <CodePane
            textSize={20}
            lang="jsx"
            source={require('raw-loader!../assets/snippets/mainAlgo.snip')}
            margin="20px auto"
            overflow="overflow"
          />
        </Slide>*/}

        {/* Algorytm - uniwersalność */}
        <Slide notes={notes.algoUniversal}>
          <Heading size={1} fit caps>Uniwersalność algorytmu</Heading>
          <div style={{display: 'flex', justifyContent: 'center', textAlign: 'left'}}>
            <List>
              <Appear><ListItem>Dodanie nowej rzeczy / kategorii wymaga bardzo małych zmian</ListItem></Appear>
              <Appear><ListItem>Wydzielone miejsce do obsługi rzeczy / kategorii specjalnych</ListItem></Appear>
            </List>
          </div>
        </Slide>

        {/* Implementacja - ReactNative */}
        <Slide notes={notes.reactNative} bgColor="bgReact">
          <Image className="logo-bg" src={images.frameworks.react}/>
          <div style={{display: 'flex', justifyContent: 'center'}}>
            <Image className="react-logo" src={images.frameworks.react}/>
            <Heading size={1} fill textColor="white">ReactNative</Heading>
          </div>
          <div style={{display: 'flex', justifyContent: 'center', textAlign: 'left'}}>
            <List>
              <Appear><ListItem>JavaScript (JSX)</ListItem></Appear>
              <Appear><ListItem>Android, iOS, Windows 10</ListItem></Appear>
              <Appear><ListItem>Stworzony przez Facebooka</ListItem></Appear>
              <Appear><ListItem>Mało specjalistów na rynku w Polsce</ListItem></Appear>
            </List>
          </div>
        </Slide>

        {/* Implementacja - Redux */}
        <Slide notes={notes.redux} bgColor="bgRedux">
          <Image className="logo-bg" src={images.frameworks.redux}/>
          <div style={{display: 'flex', justifyContent: 'center'}}>
            <Image className="redux-logo" src={images.frameworks.redux}/>
            <Heading size={1} fill textColor="white">Redux</Heading>
          </div>
          <div style={{display: 'flex', justifyContent: 'center', textAlign: 'left'}}>
            <List>
              <Appear><ListItem>Zarządzanie stanem</ListItem></Appear>
              <Appear><ListItem>Wszystkie zmienne w jednym miejscu</ListItem></Appear>
              <Appear><ListItem>Podróże w czasie</ListItem></Appear>
              <Appear><ListItem>Często używany z Reactem</ListItem></Appear>
            </List>
          </div>
        </Slide>

        {/* UI - ekrany */}
        <Slide notes={notes.screens} className="screens">
          <Heading size={5} fill caps>Android Emulator</Heading>
          <div style={{display: 'flex', flexDirection: 'row', justifyContent: 'space-between'}}>
            <Image src={images.thesis.newTripScreen}/>
            <Image src={images.thesis.packingListScreen}/>
          </div>
        </Slide>

        {/* Wieloplatformowość */}
        <Slide notes={notes.platforms} className="screens">
          <Image className="androbot" src={images.backgrounds.androbot}/>
          <Heading size={5} fill caps>Android</Heading>
          <div style={{display: 'flex', flexDirection: 'row', justifyContent: 'space-between'}}>
            <Image src={images.thesis.android1}/>
            <Image src={images.thesis.android2}/>
          </div>
        </Slide>

        {/* Wieloplatformowość */}
        <Slide notes={notes.platforms} className="screens" bgImage={images.backgrounds.sierra.replace('/','')} bgDarken={.25}>
          <Heading size={5} fill caps>iOS - symulator</Heading>
          <div style={{display: 'flex', flexDirection: 'row', justifyContent: 'space-between'}}>
            <Image src={images.thesis.vboxios1}/>
            <Image src={images.thesis.vboxios2}/>
          </div>
        </Slide>

        {/* Wieloplatformowość */}
        <Slide notes={notes.platforms} className="screens">
          <Heading size={5} fill caps>iOS</Heading>
          <div style={{display: 'flex', flexDirection: 'row', justifyContent: 'space-between'}}>
            <Image src={images.thesis.ios1}/>
            <Image src={images.thesis.ios2}/>
          </div>
        </Slide>

        {/* Wieloplatformowość */}
        <Slide notes={notes.platforms} className="screens" bgImage={images.backgrounds.w10.replace('/','')} bgDarken={.25}>
          <Heading size={5} fill caps textColor="white">Windows</Heading>
          <div style={{display: 'flex', flexDirection: 'row', justifyContent: 'space-between'}}>
            <Image src={images.thesis.windows1}/>
            <Image src={images.thesis.windows2}/>
          </div>
        </Slide>

        {/* Co poprawić */}
        <Slide notes={notes.improvements}>
          <Heading size={1} fit caps>Co można poprawić</Heading>
          <div style={{display: 'flex', justifyContent: 'center', textAlign: 'left'}}>
            <List>
              <Appear><ListItem>Dodać więcej przedmiotów, kategorii i pytań</ListItem></Appear>
              <Appear><ListItem>Baza danych</ListItem></Appear>
              <Appear><ListItem>Interaktywność listy</ListItem></Appear>
            </List>
          </div>
        </Slide>

        {/* Co zyskałem ?*/}

        <Slide transition={["slide", "fade"]} bgDarken={0.75}>
          <Image className="planet" src={images.backgrounds.travels}/>
          <Heading size={1} textColor="white" style={{position: 'relative'}} fit>Dziękuję za uwagę</Heading>
        </Slide>
      </Deck>
    );
  }
}
